This is a project created to complete a homework task.



A task description:
Using Google geocoding API get X Europe cities information (take city names from csv file).
1. Send HTTP GET method towards the API, receive JSON response and save it to database.
 (task include: describe POJO classes, save data in Postgres, describe entities using Persistance API, implement threading)

2. Create text UI so that user can provide object coordinates and get city name if the object exist in boundaries of a city.
If object does not exist return 10 closest cities sorded by distance from closest to most distanced.



Implementation:
1. Depending on arguments jar file can import data or get data from database:
        2 arguments for import: 'import' '/path/to/csv/file' - will get records from csv file, call API and save result to database.
        3 arguments to get data: 'get' 'latitude' 'longtitude' - will get city or 10 closest cities depends on location of searched object



How to use:
1. download project and change application.yml file content to your information
2. build jar file (
3. test using java-11 "/path/to/java/11" -jar "/path/to/jar/file.jar" argument1 argument2 etc.. (look "Implementetion above")



Extra:
csv file content example:
name_en;name_en
Vienna;Austria
Waterloo;Belgium
Genval;Belgium
Trogir;Croatia
Makarska;Croatia
Split;Croatia
Opatija;Croatia
Sibenik;Croatia
Cavtat;Croatia
Korenica;Croatia
Rijeka;Croatia
Dubrovnik;Croatia
Rovinj;Croatia
Podstrana;Croatia
Pula;Croatia
Porec;Croatia
Omisali;Croatia
Paphos;Cyprus
Limassol;Cyprus
Kyrenia;Cyprus
Larnaca;Cyprus
Protaras;Cyprus
Nicosia;Cyprus
Kissonerga;Cyprus
Paralimni;Cyprus
Brno;Czech Republic
Prague;Czech Republic
Aarhus;Denmark
Billund;Denmark
Copenhagen;Denmark
Ronne;Denmark
Kastrup;Denmark
Tallinn;Estonia
Parnu;Estonia
Tartu;Estonia
Tampere;Finland
Helsinki;Finland
