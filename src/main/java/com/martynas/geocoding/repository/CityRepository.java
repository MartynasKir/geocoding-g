package com.martynas.geocoding.repository;

import com.martynas.geocoding.entity.db_entity.CityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<CityEntity, Long> {

    boolean existsCityEntityByFormattedAdress(String cityAdress);

}
