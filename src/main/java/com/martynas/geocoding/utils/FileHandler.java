package com.martynas.geocoding.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileHandler {
    String filePath;

    public FileHandler(String filePath){
        this.filePath = filePath;
    }

    public List<String> getFileContent(){
        List<String> valuesList = new ArrayList<>();
        File file = new File(filePath);

        try	{
            Scanner inputStream = new Scanner(file);
            inputStream.next(); // skip the first line in file
            while (inputStream.hasNext()){
                String dataLine = inputStream.next();
                valuesList.add(dataLine);
            }
            return valuesList;
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
        return valuesList;
    }
}
