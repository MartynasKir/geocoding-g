package com.martynas.geocoding.entity.web_entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true) //ingore JSON atributes not listed in a class
public class Geometry {

    private Viewport viewport;
    private Coordinates location;

    @JsonCreator
    public Geometry(@JsonProperty("viewport") Viewport viewport,
                    @JsonProperty("location") Coordinates location) {
        this.viewport = viewport;
        this.location = location;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public Coordinates getLocation() {
        return location;
    }
}
