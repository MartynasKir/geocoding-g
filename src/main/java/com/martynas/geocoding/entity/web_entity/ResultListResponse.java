package com.martynas.geocoding.entity.web_entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true) //ingore atributes not listed in a class
public class ResultListResponse {

    private List<Result> results;
    private String status;

//Response from endpoint
    @JsonCreator
    public ResultListResponse(@JsonProperty("results") List<Result> results,
                              @JsonProperty("status") String status) {
        this.results = results;
        this.status = status;
    }

    public List<Result> getResults() {
        return results;
    }

    public String getStatus() {
        return status;
    }
}
