package com.martynas.geocoding.entity.web_entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true) //ingore atributes not listed in a class
public class Result {
    private String formattedAdress;
    private Geometry geometry;

    @JsonCreator
    public Result(@JsonProperty("formatted_address") String formattedAdress,
                  @JsonProperty("geometry") Geometry geometry) {
        this.formattedAdress = formattedAdress;
        this.geometry = geometry;
    }

    public String getFormattedAdress() {
        return formattedAdress;
    }

    public Geometry getGeometry() {
        return geometry;
    }
}
