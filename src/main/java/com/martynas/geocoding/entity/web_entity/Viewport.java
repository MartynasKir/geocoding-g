package com.martynas.geocoding.entity.web_entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true) //ingore atributes not listed in a class
public class Viewport {
    private Coordinates northeast;
    private Coordinates southwest;

    @JsonCreator
    public Viewport(@JsonProperty("northeast") Coordinates northeast,
                    @JsonProperty("southwest") Coordinates southwest) {
        this.northeast = northeast;
        this.southwest = southwest;

    }

    public Coordinates getNortheast() {
        return northeast;
    }

    public Coordinates getSouthwest() {
        return southwest;
    }

}
