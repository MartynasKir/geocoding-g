package com.martynas.geocoding.entity.web_entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true) //ingore atributes not listed in a class
public class Coordinates {
    private Double latitude;
    private Double longtitude;

    @JsonCreator
    public Coordinates(@JsonProperty("lat") Double latitude,
                       @JsonProperty("lng") Double longtitude) {
        this.latitude = latitude;
        this.longtitude = longtitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongtitude() {
        return longtitude;
    }
}
