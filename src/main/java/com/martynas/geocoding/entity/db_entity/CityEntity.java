package com.martynas.geocoding.entity.db_entity;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "city")
public class CityEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id",nullable=false, updatable=false)
    private long id;

    @Column (name = "formatted_adress",nullable=false)
    private String formattedAdress;

    @Column (name = "city_center_latitude",nullable=false)
    private Double cityCenterLatitude;

    @Column (name = "city_center_longtitude",nullable=false)
    private Double cityCenterLongtitude;

    @Column (name = "southwest_latitude",nullable=false)
    private Double southWestLatitude;

    @Column (name = "southwest_longtitude",nullable=false)
    private Double southWestLongtitude;

    @Column (name = "northeast_latitude",nullable=false)
    private Double northEastLatitude;

    @Column (name = "northeast_longtitude",nullable=false)
    private Double northEastLongtitude;


    public CityEntity() {
        //Default constructor
    }

    public CityEntity(String formattedAdress, Double cityCenterLatitude, Double cityCenterLongtitude, Double southWestLatitude, Double southWestLongtitude, Double northEastLatitude, Double northEastLongtitude) {
        this.formattedAdress = formattedAdress;
        this.cityCenterLatitude = cityCenterLatitude;
        this.cityCenterLongtitude = cityCenterLongtitude;
        this.southWestLatitude = southWestLatitude;
        this.southWestLongtitude = southWestLongtitude;
        this.northEastLatitude = northEastLatitude;
        this.northEastLongtitude = northEastLongtitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFormattedAdress() {
        return formattedAdress;
    }

    public void setFormattedAdress(String formattedAdress) {
        this.formattedAdress = formattedAdress;
    }

    public Double getCityCenterLatitude() {
        return cityCenterLatitude;
    }

    public void setCityCenterLatitude(Double cityCenterLatitude) {
        this.cityCenterLatitude = cityCenterLatitude;
    }

    public Double getCityCenterLongtitude() {
        return cityCenterLongtitude;
    }

    public void setCityCenterLongtitude(Double cityCenterLongtitude) {
        this.cityCenterLongtitude = cityCenterLongtitude;
    }

    public Double getSouthWestLatitude() {
        return southWestLatitude;
    }

    public void setSouthWestLatitude(Double southWestLatitude) {
        this.southWestLatitude = southWestLatitude;
    }

    public Double getSouthWestLongtitude() {
        return southWestLongtitude;
    }

    public void setSouthWestLongtitude(Double southWestLongtitude) {
        this.southWestLongtitude = southWestLongtitude;
    }

    public Double getNorthEastLatitude() {
        return northEastLatitude;
    }

    public void setNorthEastLatitude(Double northEastLatitude) {
        this.northEastLatitude = northEastLatitude;
    }

    public Double getNorthEastLongtitude() {
        return northEastLongtitude;
    }

    public void setNorthEastLongtitude(Double northEastLongtitude) {
        this.northEastLongtitude = northEastLongtitude;
    }
}
