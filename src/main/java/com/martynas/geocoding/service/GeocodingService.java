package com.martynas.geocoding.service;

import com.martynas.geocoding.config.GeocodingProps;
import com.martynas.geocoding.entity.db_entity.CityEntity;
import com.martynas.geocoding.entity.web_entity.Coordinates;
import com.martynas.geocoding.entity.web_entity.ResultListResponse;
import com.martynas.geocoding.repository.CityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class GeocodingService {

    private static Logger log = LoggerFactory.getLogger(GeocodingService.class);

    private static final String GEOCODING_ADRESS_ENDPOINT = "maps/api/geocode/json?address=";
    private final String baseUrl;
    private final String apiKey;
    private final RestTemplate restTemplate;
    private final HttpHeaders headers;

    @Autowired
    private GeocodingProps geocodingProps;

    @Autowired
    private CityRepository cityRepository;

    public GeocodingService(GeocodingProps geocodingProps) {

        this.restTemplate = new RestTemplate();
        this.headers = new HttpHeaders();
        this.geocodingProps = geocodingProps;
        this.baseUrl = geocodingProps.getGeocodingUrl();
        this.apiKey = geocodingProps.getApiKey();
    }

    @Async("asyncExecutor")
    public void getCityInfoAndImportToDB(List<String> list) {

        for (int i = 0; i < list.size(); i++) {
            String[] values = list.get(i).split(";");
            String city = values[0];
            String country = values[1];
            String callUrl = baseUrl + GEOCODING_ADRESS_ENDPOINT + city + ",+" + country + "&key=" + apiKey;

            log.info("Calling url: {}", callUrl);

            HttpEntity<String> entity = new HttpEntity<>(headers);
            ResponseEntity<ResultListResponse> response = restTemplate.exchange(callUrl, HttpMethod.GET, entity, ResultListResponse.class);

            String formattedAdress = response.getBody().getResults().get(0).getFormattedAdress();
            Coordinates location = response.getBody().getResults().get(0).getGeometry().getLocation();
            Coordinates northEast = response.getBody().getResults().get(0).getGeometry().getViewport().getNortheast();
            Coordinates southWest = response.getBody().getResults().get(0).getGeometry().getViewport().getSouthwest();


            if (cityRepository.existsCityEntityByFormattedAdress(formattedAdress)) {
                log.info("City: {} already exist in database", formattedAdress);
            } else {
                log.info("Saving city '{}' into database.",formattedAdress);

                CityEntity cityEntity = new CityEntity(
                        formattedAdress,
                        location.getLatitude(),
                        location.getLongtitude(),
                        southWest.getLatitude(),
                        southWest.getLongtitude(),
                        northEast.getLatitude(),
                        northEast.getLongtitude());

                cityRepository.save(cityEntity);
                log.info("Record saved");
            }
        }

        System.exit(0);
    }

    public void printCityOrCities(Double latitude, Double longtitude) {

        List<CityEntity> cityList = cityRepository.findAll();

        for (int i = 0; i < cityList.size(); i++) {
            CityEntity city = cityList.get(i);
            if (locationExistInCity(city, latitude, longtitude)) {

                log.info("Object [{}, {}] is in city: {}"
                        , city.getCityCenterLatitude()
                        , city.getCityCenterLatitude()
                        , city.getFormattedAdress());

                System.exit(0);
            }
        }

        // If location is not in city this method will run
        log.info("Looking for 10 closest cities");
        printTenClosestCities(cityList, latitude, longtitude);
    }


    private void printTenClosestCities(List<CityEntity> cityList, Double latitude, Double longtitude) {

        LinkedHashMap<String, Double> cityDistanceHashMap = new LinkedHashMap<>();
        LinkedHashMap<String, Double> sortedCityDistanceHashMap = new LinkedHashMap<>();

        for (int i = 0; i < cityList.size(); i++) {
            CityEntity city = cityList.get(i);
            Double distance = getDistanceInKm(latitude, longtitude, city.getCityCenterLatitude(), city.getCityCenterLongtitude());
            cityDistanceHashMap.put(city.getFormattedAdress(), distance);
        }

        cityDistanceHashMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(x -> sortedCityDistanceHashMap.put(x.getKey(), x.getValue()));

        Set<Map.Entry<String, Double>> entries = sortedCityDistanceHashMap.entrySet();
        Integer counter = 0;
        for (Map.Entry<String, Double> entry : entries) {
            counter++;

            Long roundedValue = Math.round(entry.getValue()*100)/100;
            System.out.println("City: "+entry.getKey()+ ". Distance: "+roundedValue + "km");
            if (counter == 10){
                break;
            }
        }
        System.exit(0);
    }


        public boolean locationExistInCity (CityEntity cityEntity, Double latitude, Double longtitude){
            log.info("Checking: {}", cityEntity.getFormattedAdress());

            if (!(cityEntity.getSouthWestLongtitude() <= longtitude && longtitude <= cityEntity.getNorthEastLongtitude())) {
                log.info("Object longtitude [{}, {}] not in boundaries of : {}", latitude, longtitude, cityEntity.getFormattedAdress());
                return false;

            } else {
                if (!(cityEntity.getSouthWestLatitude() <= latitude && latitude <= cityEntity.getNorthEastLatitude())) {
                    log.info("Object latitude and longtitude [{}, {}] not in boundaries of : {}", latitude, longtitude, cityEntity.getFormattedAdress());
                    return false;
                } else {
                    log.info("Location [{}, {}] is in the city : {}", latitude, longtitude, cityEntity.getFormattedAdress());
                    return true;
                }
            }
        }

        private double getDistanceInKm ( double lat1, double lon1, double lat2, double lon2){
            // Credit goes to user "dommer" from https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude
            double theta = lon1 - lon2;
            double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515; //miles
            dist = dist * 1.609344; // kilometers
            return (dist);
        }

        // Converts decimal degrees to radians
        private double deg2rad ( double deg){
            return (deg * Math.PI / 180.0);
        }

        // Converts radians to decimal degrees
        private double rad2deg ( double rad){
            return (rad * 180.0 / Math.PI);
        }


}
