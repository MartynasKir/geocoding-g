package com.martynas.geocoding;

import com.martynas.geocoding.service.GeocodingService;
import com.martynas.geocoding.utils.FileHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.List;

@SpringBootApplication
@EnableAsync
public class Main implements CommandLineRunner {

	@Autowired
	private GeocodingService service;

	public static void main(String[] args) throws Exception {
		SpringApplication app = new SpringApplication(Main.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.setWebApplicationType(WebApplicationType.NONE);
		app.run(args);
	}

	@Override
	public void run(String... args) throws Exception {
		for(String arg:args){
			System.out.println("Args given: "+arg);
		}

		if (args.length == 0){
			System.out.println("No arguments provided. Please provide following arguments:\n" +
					"'import' '/filepath' OR 'get' 'latitude' 'longtitude'");

		} else if (args[0].compareTo("import")==0){
			if (args.length == 2){
				System.out.println("Importing...");
				String filePath = args[1].replace("\\","\\\\");

				FileHandler handler = new FileHandler(filePath);
				List<String> list = handler.getFileContent();

				service.getCityInfoAndImportToDB(list);

			} else {
				System.out.println("Please provide 2 arguments for 'import' keyword: \n" +
						" 'import' '/filepath'");
			}


		} else if (args[0].compareTo("get")==0){
			if (args.length == 3){
				System.out.println("Checking given coordinates");
				Double latitude = Double.parseDouble(args[1]);
				Double longtitude = Double.parseDouble(args[2]);

				service.printCityOrCities(latitude,longtitude);

			} else {
				System.out.println("Please provide 3 arguments for 'get' keyword: \n" +
						"'get' 'latitude' 'longtitude'");
			}

		} else {
			System.out.println("First argument is not 'import' nor 'get'. Please provide correct arguments");
		}
	}
}
