package com.martynas.geocoding.service;

import com.martynas.geocoding.config.GeocodingProps;
import com.martynas.geocoding.entity.db_entity.CityEntity;
import org.junit.Assert;
import org.junit.Test;

public class GeocodingServiceTest {

    GeocodingProps geocodingProps = new GeocodingProps();
    GeocodingService service = new GeocodingService(geocodingProps);
    Double inLatitude = 48.255;
    Double inLongtitude = 16.501;
    Double outLatitude = 49.255;
    Double outLongtitude = 17.501;

    CityEntity city = new CityEntity(
            "Vienna, Austria",
            48.208,
            16.374,
            48.118,
            16.183,
            48.323,
            16.577);



    @Test
    public void getCityInfoAndImportToDB_TestIfPrintTenClosestCitiesMethodDontGetCalled() {

    }
    @Test
    public void getCityInfoAndImportToDB_TestIfPrintTenClosestCitiesMethodGetCalled() {

    }

    @Test
    public void printCityOrCities() {
    }

    @Test
    public void returnTrueIfLocationExistInCity() {

        Boolean result = service.locationExistInCity(city,inLatitude,inLongtitude);

        Assert.assertEquals(true, result);

    }
    @Test
    public void returnFalseIfLocationExistInCity() {

        Boolean result = service.locationExistInCity(city,outLatitude,outLongtitude);

        Assert.assertEquals(false,result);
    }
}